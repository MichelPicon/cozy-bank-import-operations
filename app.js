var express			= require('express'),
	path			= require('path'),
	cookieParser	= require('cookie-parser'),
	logger			= require('morgan');

var authRouter			= require('./routes/auth');
var accountsRouter		= require('./routes/accounts');
var operationsRouter	= require('./routes/operations');

var app = express();

app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', authRouter);
app.use('/auth', authRouter);
app.use('/accounts', accountsRouter);
app.use('/operations', operationsRouter);

app.use(function(req, res, next) {
	var err = new Error('Page Not Found');
	err.status = 404;
	next(err);
});

app.use(function(req, res, next) {
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};
	
	res.status(err.status || 500);
	res.render('error');
});

module.exports = app;
