var express	= require('express'),
	router	= express.Router(),
	https	= require('https'),
	UUID	= require('uuid-1345');

var state	= "";

var server					= "",
	clientid				= "",
	clientsecret			= "",
	registrationaccesstoken	= "",
	accesstoken				= "";

var scope = (process.env.SCOPE || "io.cozy.bank.*:ALL");

UUID.v4(function (err, result) {
    state = result;
});

router.get('/', function(req, res, next) {
	if(typeof req.query.code === 'undefined') {
		res.render('serverChoice');
	} else {
		const data =	"grant_type=authorization_code&code=" + req.query.code +
						"&client_id=" + clientid +
						"&client_secret=" + clientsecret;

		const options = {
			hostname: server,
			path: '/auth/access_token',
			method: 'POST',
			headers: {
				'Host': server,
				'Accept': 'application/json',
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		};

		const accessTokenRequest = https.request(options, accessTokenResponse => {
			accessTokenResponse.on('data', d => {
				var r = JSON.parse(d);
				accesstoken = r.access_token;
				res.redirect('/accounts?server=' + server + '&accesstoken=' + accesstoken);
			});
		});

		accessTokenRequest.on('error', error => {
			console.error('accessTokenRequest:'+error)
		});

		accessTokenRequest.write(data);
		accessTokenRequest.end();
	}
});

router.post('/', function(req, res, next) {
	if(typeof req.body.server !== 'undefined') {
		server = req.body.server;
	} else {
		var err = new Error('Server Not Found');
		err.status = 404;
		next(err);
	}
	
	const data = JSON.stringify({
		redirect_uris: ["http://localhost:" + (process.env.PORT || '3000')],
		client_name: 'cozycli',
		software_id: 'cozycli'
	})

	const options = {
		hostname: server,
		path: '/auth/register',
		method: 'POST',
		headers: {
			'Host': server,
			'Accept': 'application/json',
			'Content-Type': 'application/json',
			'Content-Length': data.length
		}
	}

	const registerRequest = https.request(options, registerResponse => {
		registerResponse.on('data', d => {
			var r = JSON.parse(d);
			clientid = r.client_id;
			clientsecret = r.client_secret;
			registrationaccesstoken = r.registration_access_token;
			res.redirect(307, '/login');
		});
	});

	registerRequest.on('error', error => {
		console.error('registerRequest:'+error)
	});

	registerRequest.write(data);
	registerRequest.end();
});

router.post('/login', function(req, res, next) {
	var loginURL =	"https://" + server + "/auth/login?redirect=" + 
	encodeURIComponent(
		"https://" + server + "/auth/authorize?client_id=" + clientid +
		"&response_type=code&scope=" + scope + "&state=" + state +
		"&redirect_uri=http://localhost:" + (process.env.PORT || '3000'));

	res.redirect(loginURL);
});

router.get('/disconnect', function(req, res, next) {
	const options = {
		hostname: server,
		path: '/auth/register/' + clientid,
		method: 'DELETE',
		headers: {
			'Origin': 'https://' + server,
			'Authorization': 'Bearer ' + registrationaccesstoken
		}
	}
	
	const revokeRequest = https.request(options, revokeResponse => {
		revokeResponse.on('data', d => {
		});
		revokeResponse.on('close', d => {
			res.redirect('/');
		});
		revokeResponse.on('end', d => {
		});
	});

	revokeRequest.on('error', error => {
		console.error('revokeRequest:'+error)
	});

	revokeRequest.end();
})

module.exports = router;
