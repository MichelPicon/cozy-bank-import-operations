var express		= require('express'),
	router		= express.Router(),
	https		= require('https'),
	formidable	= require('formidable'),
	lineByLine	= require('n-readlines');

router.get('/', function(req, res, next) {
	var server		= req.query.server,
		accesstoken	= req.query.accesstoken,
		accountId	= req.query.accountId;
	
	var operations = [];
	
	const options = {
		hostname: server,
		path: '/data/io.cozy.bank.operations/_all_docs?include_docs=true',
		method: 'GET',
		headers: {
			'Origin': 'https://' + server,
			'Authorization': 'Bearer ' + accesstoken,
			'Accept': 'application/json'
		}
	}

	const operationsRequest = https.request(options, operationsResponse => {
		var datas = "";
		operationsResponse.on('data', d => {
			datas += d;
		});
		
		operationsResponse.on('end', d => {
			var r = JSON.parse(datas);
			var tmpOperations = r.rows;
			
			for(i=0;i<tmpOperations.length;i++) {
				if(	typeof tmpOperations[i].doc !== 'undefined' &&
					tmpOperations[i].doc.account === accountId) {
						console.log(tmpOperations[i].doc);
						operations.push({
							id		: tmpOperations[i].doc._id,
							label	: tmpOperations[i].doc.label,
							date	: tmpOperations[i].doc.date,
							amount	: tmpOperations[i].doc.amount
						});
				}
			}
			
		    res.render('operations.ejs', {
		    	server		: server,
		    	accesstoken	: accesstoken,
		    	accountId	: accountId,
		    	operations	: operations.sort(function(a, b) {
		    		var dtA = new Date(a.date);
		    		var dtB = new Date(b.date);
		    		
		    		return dtB-dtA;
		    	})
		    });
		});
	});

	operationsRequest.on('error', error => {
		console.error('operationsRequest:'+error)
	});

	operationsRequest.end();
});

router.get('/import/view', function(req, res, next) {
	var server		= req.query.server,
		accesstoken	= req.query.accesstoken,
		accountId	= req.query.accountId;
	
	var accountLabel = "";
	
	const options = {
		hostname: server,
		path: '/data/io.cozy.bank.accounts/_all_docs?include_docs=true',
		method: 'GET',
		headers: {
			'Origin': 'https://' + server,
			'Authorization': 'Bearer ' + accesstoken,
			'Accept': 'application/json'
		}
	}

	const accountsRequest = https.request(options, accountsResponse => {
		var datas = "";
		accountsResponse.on('data', d => {
			datas += d;
		});
		
		accountsResponse.on('end', d => {
			var r = JSON.parse(datas);
			var tmpAccounts = r.rows;
			
			for(i=0;i<tmpAccounts.length;i++) {
				if(	typeof tmpAccounts[i].doc !== 'undefined' &&
					typeof tmpAccounts[i].doc.label !== 'undefined' &&
					tmpAccounts[i].doc.id === accountId) {
						accountLabel = tmpAccounts[i].doc.label
				}
			}
			
			res.render('import.ejs', {
				server: server,
				accesstoken: accesstoken,
				accountId: accountId,
				accountLabel: accountLabel
			});
		});
	});

	accountsRequest.on('error', error => {
		console.error('accountsRequest:'+error)
	});

	accountsRequest.end();
});

router.post('/import', function(req, res, next) {
	var form = new formidable.IncomingForm();
	form.parse(req, function (err, fields, files) {
		var server		= fields.server,
			accesstoken	= fields.accesstoken,
			accountId	= fields.accountId;

		const options = {
			hostname: server,
			path: '/data/io.cozy.bank.operations/',
			method: 'POST',
			headers: {
				'Origin': 'https://' + server,
				'Authorization': 'Bearer ' + accesstoken,
				'Accept': 'application/json'
			}
		};
		
		const liner = new lineByLine(files.fileupload.path);
		var line;

		while (line = liner.next()) {
			var arr = line.toString().split(";");

			const data = JSON.stringify({
				account				: accountId,
				date				: formatDate2(arr[0]),
				manualCategoryId	: arr[2],
				label				: arr[1],
				amount				: parseFloat(arr[3]),
				currency			: arr[4],
				deleted				: null,
				isActive			: true,
				isComing			: false,
				metadata			: {
					dateImport: (new Date()).toJSON()
				},
				originalBankLabel	: arr[2],
				rawDate				: formatSimpleDate(arr[0]),
				realisationDate		: formatDate2(arr[0]),
				toCategorize		: false,
				valueDate			: formatSimpleDate(arr[0])
			});
			
			manageArrayOperation(options, data);

		}
		
		res.redirect('/accounts?server=' + server + '&accesstoken=' + accesstoken);
	});
});

router.get('/delete', function(req, res, next) {
	var server		= req.query.server,
		accesstoken	= req.query.accesstoken,
		accountId	= req.query.accountId;
	
	const options = {
		hostname: server,
		path: '/data/io.cozy.bank.operations/_all_docs?include_docs=true',
		method: 'GET',
		headers: {
			'Origin': 'https://' + server,
			'Authorization': 'Bearer ' + accesstoken,
			'Accept': 'application/json'
		}
	}

	const operationsRequest = https.request(options, operationsResponse => {
		var datas = "";
		operationsResponse.on('data', d => {
			datas += d;
		});
		
		operationsResponse.on('end', d => {
			var r = JSON.parse(datas);
			var tmpOperations = r.rows;
			
			for(var i=0;i<tmpOperations.length;i++) {
				if(	typeof tmpOperations[i].doc !== 'undefined' &&
					tmpOperations[i].doc.account === accountId) {
						var options = {
							hostname: server,
							path: '/data/io.cozy.bank.operations/' + tmpOperations[i].doc._id + '?rev=' + tmpOperations[i].doc._rev,
							method: 'DELETE',
							headers: {
								'Origin': 'https://' + server,
								'Authorization': 'Bearer ' + accesstoken,
								'Accept': 'application/json'
							}
						};
							
						manageArrayOperation(options, JSON.stringify({}));
				}
			}
			res.redirect('/accounts?server=' + server + '&accesstoken=' + accesstoken);
		});
	});

	operationsRequest.on('error', error => {
		console.error('operationsRequest:'+error)
	});

	operationsRequest.end();
});


function manageArrayOperation(options, data) {
	const postRequest = https.request(options, postResponse => {
		postResponse.on('data', d => {
			if((new String(postResponse.statusCode)).startsWith('20')) {
				console.log('data:'+d);
			} else {
				setTimeout(function(){
					manageArrayOperation(options, data);
				}, 2000);
			}
		});
		postResponse.on('error', error => {
			console.error('error:'+error);
		});
	});
	postRequest.on('error', error => {
		console.error('postRequest:' + error + ' - ' + options + ' - ' + data)
	});

	postRequest.write(data);
	postRequest.end();
}

function formatDate1(dt) {
	var arr = dt.split("/");

	var formattedDt = new Date();
	formattedDt.setDate(arr[0]);
	formattedDt.setMonth(arr[1]-1);
	formattedDt.setFullYear(arr[2]);
	formattedDt.setMilliseconds(0);
	formattedDt.setSeconds(0);
	formattedDt.setMinutes(0);
	formattedDt.setHours(12);
	
	//2020-04-28T12:00:00.000Z
	return formattedDt;
}

function formatSimpleDate(dt) {
	var arr = dt.split("/");
	
	//2020-04-28
	return (arr[2] + '-' + arr[1] + '-' + arr[0]);
}

function formatDate2(dt) {
	var arr = dt.split("/");
	
	//2020-04-28T00:00:00.000Z
	return (arr[2] + '-' + arr[1] + '-' + arr[0] + 'T12:00:00.000Z');
}

module.exports = router;
