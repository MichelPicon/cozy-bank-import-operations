var express	= require('express'),
	router	= express.Router(),
	https	= require('https');

router.get('/', function(req, res, next) {
	var server		= req.query.server,
		accesstoken	= req.query.accesstoken;
	
	var accounts = [];
	
	const options = {
		hostname: server,
		path: '/data/io.cozy.bank.accounts/_all_docs?include_docs=true',
		method: 'GET',
		headers: {
			'Origin': 'https://' + server,
			'Authorization': 'Bearer ' + accesstoken,
			'Accept': 'application/json'
		}
	}

	const accountsRequest = https.request(options, accountsResponse => {
		var datas = "";
		accountsResponse.on('data', d => {
			datas += d;
		});
		
		accountsResponse.on('end', d => {
			var r = JSON.parse(datas);
			var tmpAccounts = r.rows;
			
			for(i=0;i<tmpAccounts.length;i++) {
				if(	typeof tmpAccounts[i].doc !== 'undefined' &&
					typeof tmpAccounts[i].doc.label !== 'undefined' &&
					(tmpAccounts[i].doc.type.includes('Checkings') || tmpAccounts[i].doc.type.includes('Savings'))) {
					accounts.push({
						id: tmpAccounts[i].doc.id,
						label: tmpAccounts[i].doc.label
					});
				}
			}
			
		    res.render('accounts.ejs', {
		    	server		: server,
		    	accesstoken	: accesstoken,
		    	accounts	: accounts
		    });
		});
	});

	accountsRequest.on('error', error => {
		console.error('accountsRequest:'+error)
	});

	accountsRequest.end();
});

module.exports = router;
