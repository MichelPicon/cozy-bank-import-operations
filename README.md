# Cozy Bank Import Operations

L'objectif de cet outil est de permettre l'import d'opérations bancaires à partir d'un fichier vers son compte Cozy Bank (https://cozy.io/).

## Pre-requis
Installer [Node.js](https://nodejs.org/)

## Installation

## Dependences

## Variable d'environnement
* SCOPE : Gère les permissions nécessaire [voir doc Cozy](https://docs.cozy.io/en/cozy-stack/permissions/) (par défaut : io.cozy.bank.*:ALL)
* PORT : précise le port d'écoute du serveur (par défaut : 3000)

## Format du fichier d'entrée pour l'import
Les caractères fin de ligne doivent être au format UNIX (LF).

Le fichier d'entrée est un fichier csv, avec pour séparateur le ';', les colonnes sont les suivantes :

```
Date opération;Label;Catégorie;Montant;Devise
```

Le format de la catégorie doit être son identifiant numérique (voir [tableau des catégories Cozy](https://github.com/cozy/cozy-banks/blob/master/src/ducks/categories/tree.json))
Le plus simple, pour ne pas le gérer est d'inscrire 0, pour "uncategorized"
Le séparateur décimale pour le montant doit être un '.'
